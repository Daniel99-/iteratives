package Algorismes;

import java.util.Scanner;

public class Algorisme15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		String lletra;
		
		int aa,ee,ii,oo,uu;
		
		aa = 0;
		ee = 0;
		ii = 0;
		oo = 0;
		uu = 0;
		
		lletra = sc.nextLine();

		for (int i=0; i<lletra.length(); i++) {
			if (lletra.charAt(i) == 'a')
				aa++;
			
			else if (lletra.charAt(i) == 'e')
				ee++;
			
			else if (lletra.charAt(i) == 'i')
				ii++;
			
			else if (lletra.charAt(i) == 'o')
				oo++;
			
			else if (lletra.charAt(i) == 'u')
				uu++;
		}
		
		System.out.println("Numero de A/s: " + aa);
		System.out.println("Numero de E/s: " + ee);
		System.out.println("Numero de I/s: " + ii);
		System.out.println("Numero de O/s: " + oo);
		System.out.println("Numero de U/s: " + uu);
	}

}
