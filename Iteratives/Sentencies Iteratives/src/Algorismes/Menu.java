package Algorismes;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int num1=0, num2=0, res, opcio;
		boolean operands = false;

		do {
			System.out.println("1.- Introduir operands");
			System.out.println("2.- Sumar");
			System.out.println("3.- Restar");
			System.out.println("4.- Multiplicar");
			System.out.println("5.- Dividir");
			System.out.println("0.- Sortir");

			System.out.println("Digues que vols fer:");
			opcio = sc.nextInt();

			switch (opcio) {
			case 0:
				break;
			case 1:
				System.out.println("Digues el primer n�mero:");
				num1 = sc.nextInt();
				System.out.println("Digues el segon n�mero:");
				num2 = sc.nextInt();
				operands = true;
				break;
			case 2:
				if (operands == true) {
					res = num1 + num2;
					System.out.println(num1 + " + " + num2 + " = " + res);
				} else {
					System.out.println("Ha d'introduir el valor dels operands");
				}
				break;
			case 3:
				if (operands == true) {
					res = num1 - num2;
					System.out.println(num1 + " - " + num2 + " = " + res);
				} else {
					System.out.println("Ha d'introduir el valor dels operands");
				}
				break;
			case 4:
				if (operands == true) {
					res = num1 * num2;
					System.out.println(num1 + " * " + num2 + " = " + res);
				} else {
					System.out.println("Ha d'introduir el valor dels operands");
				}
				break;
			case 5:
				if (operands == true) {
					if (num2 != 0) {
						res = num1 / num2;
					System.out.println(num1 + " / " + num2 + " = " + res);
					}
				} else {
					System.out.println("Ha d'introduir el valor dels operands");
				}
				break;
			default:
				System.err.println("Opci� Incorrecta");
				break;
			}

		} while (opcio != 0);

	}

}


	





